####
#
# makefile for cloudformation stacks for the pitastic email setup
#
####

#
# VARIABLES
#

# name of the cloudformation stack to create
STACK_NAME?=pitastic-email

# general shortcuts
DOCKER=docker run --rm -ti --env-file ${CURDIR}/.env -v ${CURDIR}:/data -w /data
BASH=${DOCKER} --entrypoint=/bin/bash piaws -c

#
# TARGETS
#

# make all
all: docker-image cloudformation

# build docker image
docker-image:
	docker build -t piaws .

# deploy the cloudformation template
cloudformation:
	${BASH} "aws cloudformation deploy \
		--stack-name ${STACK_NAME} --template-file Cloudformation.yaml \
		--no-fail-on-empty-changeset"

