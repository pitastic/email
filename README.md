# pitastic email
E-Mails are sent and received over the swiss webhoster 'hostpoint.ch'.
For this to work we need to specify hostpoints mx servers for the pitastic.ch domains.

## Requirements
- docker
- make

## Setup
### env file
Copy the env file and insert your AWS keys etc
```
cp .env.example .env
```

### Build aws container
We use a docker container with the aws cli installed to keep our environment clean
```
make docker-image
```

### Cloudformation
With the env file and the container image in place we can execute the cloudformation stack to deploy the dns entries

```
make cloudformation
```